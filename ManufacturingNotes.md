**Manufacturing Notes**

The UPS PiHAT can be manufactured correctly by following these steps 


**1. Gather the required materials and components.** 

To complete this step, you will need to visit your nearest electronics store or purchase the components online. These components are common and low cost and should be readily available.

Please refer to the bill of materials (BOM) to get a list of all required components. 

**Link to BOM: [**https://gitlab.com/lebo_tshangani/eee3088f_project1/-/blob/758c565b1e47d3593aa05f48eb24f4d2900d80f4/BOM.md](https://gitlab.com/lebo_tshangani/eee3088f_project1/-/blob/758c565b1e47d3593aa05f48eb24f4d2900d80f4/BOM.md)**



**2. Find a suitable PCB fabricator.**

To complete this step, locate and arrange with a known PCB fabrication company to manufacture your PCB.

Ensure that you share the design files with the PCB fabrication company.

**Link to design files: <https://gitlab.com/lebo_tshangani/eee3088f_project1/-/tree/master/source_files>**



**3. Solder on the components**

Finally, after the procurement of required materials and the manufacturing of the PCB, you will need to solder on the components on to your PCB.

**NB**: The LM555, LMV331 and the LEDs are sensitive to heat so ensure that you solder them with care.
