﻿Getting started 

**Setting up the PiHAT**

Follow the following steps to ensure that the UPS PiHAT is safely setup.

- Begin by connecting the Pi charging module to the high voltage connectors available on the PiHAT. Ensure that your connections form a male-female connection pair on each end.

- Complete the PiHAT set up process by ensuring that the power plug connection is secure. Do not connect it to the wall socket in this step.


 **Connecting the PiHAT to the Pi**

Follow the following steps to ensure that the UPS PiHAT is safely connected to the Pi.

- First ensure that the PiHAT is not connected to the wall socket.

- Then connect the PiHAT power output cord from the PiHAT to the Pi input power socket.


- The next step is to connect a single pin female – female data cable from the output GPIO pin (There is only one pin) on the PiHAT to the input GPIO of the Pi (GPIO pin 3)

- Finally, you can, connect the PiHAT plug to the socket on your wall and boot up you Pi. The PiHAT will take care of the rest.


