What this PI-hat does is provide an un-interrupted power supply to the PI  by providing power to it when the main power is off/disconnected so that the PI can shutdown safely.

The bill of materials include

4 LED's
1 green LED
1 Battery
1 Flyback Regulator Circuit
1 DC-DC buck boost converter

The contributions are split between 4 main IC's
Such as:

Status LED's
--------------------------------------------------------------------------------------------------------------------
There are 5 LED's in total 4 out of the 5 LEDs represent the battery level while the last one is meant to show whether the PI s connected to the UPS or the power outlet.

If the status LED is on then the PI is connected to the UPS. This means it is getting its power from the UPS at that point in time and if its off then it is connected to the power outlet.

Each LED represents 25% of the total UPS battery level.

If 4/4 LEDs are on then the UPS battery is at 100%
If 3/4 LEDs are on then the UPS battery is at 75%
If 2/4 LEDs are on then the UPS battery is at 50%
If 1/4 LEDs are on then the UPS battery is at 25%

---------------------------------------------------------------------------------------------------------------------
