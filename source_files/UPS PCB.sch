EESchema Schematic File Version 4
LIBS:UPS PCB-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_WallPlug P1
U 1 1 60B8BF61
P 1450 1950
F 0 "P1" H 1467 2275 50  0000 C CNN
F 1 "Conn_WallPlug" H 1467 2184 50  0000 C CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MC_1,5_2-G-5.08_1x02_P5.08mm_Horizontal" H 1850 1950 50  0001 C CNN
F 3 "~" H 1850 1950 50  0001 C CNN
	1    1450 1950
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:DIODE D5
U 1 1 60B927DC
P 2650 2100
F 0 "D5" V 2604 2180 50  0000 L CNN
F 1 "DIODE" V 2695 2180 50  0000 L CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 2650 2100 50  0001 C CNN
F 3 "~" H 2650 2100 50  0001 C CNN
F 4 "Y" H 2650 2100 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 2650 2100 50  0001 L CNN "Spice_Primitive"
	1    2650 2100
	0    -1   -1   0   
$EndComp
$Comp
L Simulation_SPICE:DIODE D4
U 1 1 60B92C19
P 2650 1650
F 0 "D4" V 2700 1450 50  0000 L CNN
F 1 "DIODE" V 2600 1350 50  0000 L CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 2650 1650 50  0001 C CNN
F 3 "~" H 2650 1650 50  0001 C CNN
F 4 "Y" H 2650 1650 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 2650 1650 50  0001 L CNN "Spice_Primitive"
	1    2650 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 1800 2650 1900
Wire Wire Line
	2100 2250 2650 2250
Wire Wire Line
	2100 1500 2650 1500
Wire Wire Line
	2100 1800 2100 1900
$Comp
L Simulation_SPICE:DIODE D1
U 1 1 60B9176C
P 2100 1650
F 0 "D1" V 2100 1500 50  0000 L CNN
F 1 "DIODE" V 2000 1400 50  0000 L CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 2100 1650 50  0001 C CNN
F 3 "~" H 2100 1650 50  0001 C CNN
F 4 "Y" H 2100 1650 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 2100 1650 50  0001 L CNN "Spice_Primitive"
	1    2100 1650
	0    -1   -1   0   
$EndComp
$Comp
L Simulation_SPICE:DIODE D2
U 1 1 60B8C9CA
P 2100 2100
F 0 "D2" V 2054 2180 50  0000 L CNN
F 1 "DIODE" V 2145 2180 50  0000 L CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 2100 2100 50  0001 C CNN
F 3 "~" H 2100 2100 50  0001 C CNN
F 4 "Y" H 2100 2100 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 2100 2100 50  0001 L CNN "Spice_Primitive"
	1    2100 2100
	0    1    1    0   
$EndComp
Connection ~ 2100 2250
Wire Wire Line
	2100 1500 1850 1500
Connection ~ 2100 1500
Wire Wire Line
	1850 2050 1850 2250
Wire Wire Line
	1850 1500 1850 1850
Connection ~ 2650 1900
Wire Wire Line
	2650 1900 2650 1950
Wire Wire Line
	2100 1900 1900 1900
Wire Wire Line
	1900 1900 1900 2300
Wire Wire Line
	1900 2300 2050 2300
Connection ~ 2100 1900
Wire Wire Line
	2100 1900 2100 1950
$Comp
L power:GND #PWR02
U 1 1 60BA51E7
P 2050 2300
F 0 "#PWR02" H 2050 2050 50  0001 C CNN
F 1 "GND" H 2200 2250 50  0000 C CNN
F 2 "" H 2050 2300 50  0001 C CNN
F 3 "" H 2050 2300 50  0001 C CNN
	1    2050 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2250 1800 2250
$Comp
L power:GND #PWR01
U 1 1 60BA6537
P 1800 2250
F 0 "#PWR01" H 1800 2000 50  0001 C CNN
F 1 "GND" H 1805 2077 50  0000 C CNN
F 2 "" H 1800 2250 50  0001 C CNN
F 3 "" H 1800 2250 50  0001 C CNN
	1    1800 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 1900 2950 1900
$Comp
L power:GND #PWR04
U 1 1 60BAC97E
P 3750 2550
F 0 "#PWR04" H 3750 2300 50  0001 C CNN
F 1 "GND" H 3755 2377 50  0000 C CNN
F 2 "" H 3750 2550 50  0001 C CNN
F 3 "" H 3750 2550 50  0001 C CNN
	1    3750 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2000 2950 1900
Wire Wire Line
	2950 2200 2950 2300
Connection ~ 2950 1900
Wire Wire Line
	2950 1900 3250 1900
Wire Wire Line
	2950 2300 3250 2300
Connection ~ 2950 2300
Wire Wire Line
	2050 2300 2750 2300
Connection ~ 2050 2300
$Comp
L Device:R_Small_US R5
U 1 1 60BA8AF8
P 3050 2850
F 0 "R5" V 2845 2850 50  0000 C CNN
F 1 "1k" V 2936 2850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3050 2850 50  0001 C CNN
F 3 "~" H 3050 2850 50  0001 C CNN
	1    3050 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4600 2300 4600 2200
Wire Wire Line
	4600 2300 4600 2550
Wire Wire Line
	1650 1850 1850 1850
Wire Wire Line
	2650 1500 2850 1500
Connection ~ 2650 1500
Connection ~ 1850 2250
Wire Wire Line
	1850 2250 2100 2250
$Comp
L Device:C_Small C1
U 1 1 60BF4AE3
P 2950 2100
F 0 "C1" H 3042 2146 50  0000 L CNN
F 1 "C_Small" H 3042 2055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal" H 2950 2100 50  0001 C CNN
F 3 "~" H 2950 2100 50  0001 C CNN
	1    2950 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:Transformer_1P_1S T1
U 1 1 60BFAFB3
P 3650 2100
F 0 "T1" H 3650 2481 50  0000 C CNN
F 1 "Transformer_1P_1S" H 3650 2390 50  0000 C CNN
F 2 "Transformer_THT:Transformer_Toroid_Horizontal_D9.0mm_Amidon-T30" H 3650 2100 50  0001 C CNN
F 3 "~" H 3650 2100 50  0001 C CNN
	1    3650 2100
	1    0    0    -1  
$EndComp
Connection ~ 4600 2300
Wire Wire Line
	4100 1900 4050 1900
Wire Wire Line
	4400 1900 4500 1900
$Comp
L Device:C_Small C2
U 1 1 60BB1C23
P 4600 2100
F 0 "C2" V 4800 2050 50  0000 L CNN
F 1 "1m" V 4700 2050 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal" H 4600 2100 50  0001 C CNN
F 3 "~" H 4600 2100 50  0001 C CNN
	1    4600 2100
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:DIODE D9
U 1 1 60C08728
P 4250 1900
F 0 "D9" V 4300 1700 50  0000 L CNN
F 1 "DIODE" V 4200 1600 50  0000 L CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 4250 1900 50  0001 C CNN
F 3 "~" H 4250 1900 50  0001 C CNN
F 4 "Y" H 4250 1900 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 4250 1900 50  0001 L CNN "Spice_Primitive"
	1    4250 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 1900 4500 3100
Wire Wire Line
	4050 2300 4600 2300
Wire Wire Line
	4500 1900 4600 1900
Wire Wire Line
	4600 1900 4600 2000
Connection ~ 4500 1900
Wire Wire Line
	2950 2550 3100 2550
Wire Wire Line
	2950 2300 2950 2550
Wire Wire Line
	3300 2850 3150 2850
Wire Wire Line
	2950 2850 2800 2850
Wire Wire Line
	2800 3100 4500 3100
Wire Wire Line
	2800 2850 2800 3100
Wire Wire Line
	3500 2550 3750 2550
Connection ~ 3750 2550
Wire Wire Line
	3750 2550 4600 2550
$Comp
L Comparator:LMV331 U2
U 1 1 60C42556
P 5500 1800
F 0 "U2" H 5650 1600 50  0000 L CNN
F 1 "LMV331" H 5600 1700 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5500 1900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmv331.pdf" H 5500 2000 50  0001 C CNN
	1    5500 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1700 5050 1700
Wire Wire Line
	5400 1500 5400 1400
Wire Wire Line
	5400 1400 5050 1400
Wire Wire Line
	5050 1400 5050 1700
Connection ~ 5050 1700
Wire Wire Line
	5050 1700 5200 1700
Wire Wire Line
	2750 2600 2750 3150
Wire Wire Line
	2750 2300 2750 2500
Connection ~ 2750 2300
Wire Wire Line
	2750 2300 2950 2300
Text Label 2750 2600 0    50   ~ 0
+
Text Label 2750 2500 0    50   ~ 0
-
Wire Wire Line
	4550 2600 4650 2600
Wire Wire Line
	4650 2600 4650 2300
Wire Wire Line
	4550 3150 4550 2600
Wire Wire Line
	4550 3150 2750 3150
Wire Wire Line
	4600 3200 4600 2650
Wire Wire Line
	4600 2650 4700 2650
Wire Wire Line
	4700 2650 4700 2350
Wire Wire Line
	4650 2300 4850 2300
Connection ~ 4600 1900
Wire Wire Line
	4850 1900 4600 1900
Wire Wire Line
	4850 1900 4850 2300
Wire Wire Line
	5400 2100 5400 2150
Wire Wire Line
	4850 3000 5000 3000
$Comp
L Device:R_Small_US R3
U 1 1 60BA13E7
P 2500 3450
F 0 "R3" H 2568 3496 50  0000 L CNN
F 1 "100" H 2568 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 2500 3450 50  0001 C CNN
F 3 "~" H 2500 3450 50  0001 C CNN
	1    2500 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R6
U 1 1 60BA57EF
P 3100 3450
F 0 "R6" H 3168 3496 50  0000 L CNN
F 1 "50" H 3168 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3100 3450 50  0001 C CNN
F 3 "~" H 3100 3450 50  0001 C CNN
	1    3100 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 60BA9979
P 2500 3800
F 0 "D3" V 2539 3683 50  0000 R CNN
F 1 "LED" V 2448 3683 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 2500 3800 50  0001 C CNN
F 3 "~" H 2500 3800 50  0001 C CNN
	1    2500 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 3550 2500 3650
Wire Wire Line
	3100 3550 3100 3650
Wire Wire Line
	2500 3350 2500 3300
Wire Wire Line
	2500 3300 2800 3300
Wire Wire Line
	3100 3300 3100 3350
Wire Wire Line
	2800 3350 2800 3300
Connection ~ 2800 3300
Connection ~ 3100 3300
Wire Wire Line
	3100 3300 3400 3300
Wire Wire Line
	3400 3300 3400 3350
Wire Wire Line
	3400 3550 3400 3650
$Comp
L Device:LED D8
U 1 1 60BAF1A1
P 3400 3800
F 0 "D8" V 3439 3683 50  0000 R CNN
F 1 "LED" V 3348 3683 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 3400 3800 50  0001 C CNN
F 3 "~" H 3400 3800 50  0001 C CNN
	1    3400 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R7
U 1 1 60B9E42E
P 3400 3450
F 0 "R7" H 3468 3496 50  0000 L CNN
F 1 "25" H 3468 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3400 3450 50  0001 C CNN
F 3 "~" H 3400 3450 50  0001 C CNN
	1    3400 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 3550 2800 3650
$Comp
L Device:LED D6
U 1 1 60BAB0F1
P 2800 3800
F 0 "D6" V 2839 3683 50  0000 R CNN
F 1 "LED" V 2748 3683 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 2800 3800 50  0001 C CNN
F 3 "~" H 2800 3800 50  0001 C CNN
	1    2800 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R4
U 1 1 60BA3592
P 2800 3450
F 0 "R4" H 2868 3496 50  0000 L CNN
F 1 "75" H 2868 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 2800 3450 50  0001 C CNN
F 3 "~" H 2800 3450 50  0001 C CNN
	1    2800 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D7
U 1 1 60BAD1D5
P 3100 3800
F 0 "D7" V 3139 3683 50  0000 R CNN
F 1 "LED" V 3048 3683 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 3100 3800 50  0001 C CNN
F 3 "~" H 3100 3800 50  0001 C CNN
	1    3100 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2600 2600 2750 2600
$Comp
L Device:R_Small_US R2
U 1 1 60C636DA
P 2500 2600
F 0 "R2" V 2295 2600 50  0000 C CNN
F 1 "1k" V 2386 2600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 2500 2600 50  0001 C CNN
F 3 "~" H 2500 2600 50  0001 C CNN
	1    2500 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2350 2600 2350 3200
Connection ~ 4600 3200
Wire Wire Line
	4850 3500 4850 3000
Wire Wire Line
	5300 2650 5300 2800
Wire Wire Line
	5000 3000 5000 2650
Wire Wire Line
	5000 2650 5300 2650
Connection ~ 5000 3000
Wire Wire Line
	5000 3000 5100 3000
Text Label 4850 3450 0    50   ~ 0
+
Text Label 3650 3500 0    50   ~ 0
-
$Comp
L Device:R_Small_US R1
U 1 1 60C4EE7B
P 2250 3450
F 0 "R1" H 2318 3496 50  0000 L CNN
F 1 "20" H 2318 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 2250 3450 50  0001 C CNN
F 3 "~" H 2250 3450 50  0001 C CNN
	1    2250 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3300 2250 3300
Wire Wire Line
	2250 3300 2250 3350
Connection ~ 2500 3300
Wire Wire Line
	2350 3200 2800 3200
Wire Wire Line
	2800 3300 3100 3300
Wire Wire Line
	2800 3300 2800 3200
Connection ~ 2800 3200
Wire Wire Line
	2800 3200 4600 3200
$Comp
L Device:LED D_status1
U 1 1 60C73F3E
P 2250 3800
F 0 "D_status1" V 2000 3950 50  0000 R CNN
F 1 "LED" V 2100 3950 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm_FlatTop" H 2250 3800 50  0001 C CNN
F 3 "~" H 2250 3800 50  0001 C CNN
	1    2250 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2250 3550 2250 3650
Wire Wire Line
	2250 3950 2250 4000
Wire Wire Line
	2250 4000 2500 4000
Wire Wire Line
	3400 4000 3400 3950
Wire Wire Line
	3100 3950 3100 4000
Connection ~ 3100 4000
Wire Wire Line
	3100 4000 3400 4000
Wire Wire Line
	2800 3950 2800 4000
Connection ~ 2800 4000
Wire Wire Line
	2800 4000 3100 4000
Wire Wire Line
	2500 3950 2500 4000
Connection ~ 2500 4000
Wire Wire Line
	2500 4000 2800 4000
Wire Wire Line
	3650 4000 3400 4000
Wire Wire Line
	3650 3500 3650 4000
Connection ~ 3400 4000
Wire Wire Line
	3650 4000 5300 4000
Wire Wire Line
	5300 3400 5300 4000
Connection ~ 3650 4000
$Comp
L power:GND #PWR03
U 1 1 60CA0DDA
P 3650 4000
F 0 "#PWR03" H 3650 3750 50  0001 C CNN
F 1 "GND" H 3655 3827 50  0000 C CNN
F 2 "" H 3650 4000 50  0001 C CNN
F 3 "" H 3650 4000 50  0001 C CNN
	1    3650 4000
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LMV331 U1
U 1 1 60C59B32
P 5400 3100
F 0 "U1" H 5744 3146 50  0000 L CNN
F 1 "LMV331" H 5744 3055 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5400 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmv331.pdf" H 5400 3300 50  0001 C CNN
	1    5400 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1800 5800 1400
Wire Wire Line
	7750 3050 7750 1500
Wire Wire Line
	6000 3050 7750 3050
Wire Wire Line
	5950 1500 5900 1500
Connection ~ 6600 1950
Connection ~ 6700 3000
Wire Wire Line
	6050 1950 6600 1950
Wire Wire Line
	6050 3000 6050 1950
Wire Wire Line
	6700 3000 6050 3000
Wire Wire Line
	6100 2950 7200 2950
Wire Wire Line
	6100 2300 6100 2950
Wire Wire Line
	7200 2950 7200 2700
$Comp
L Transistor_FET:DMG2302U Q2
U 1 1 60CAEE5C
P 6150 1600
F 0 "Q2" V 6493 1600 50  0000 C CNN
F 1 "DMG2302U" V 6402 1600 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6350 1525 50  0001 L CIN
F 3 "http://www.diodes.com/assets/Datasheets/DMG2302U.pdf" H 6150 1600 50  0001 L CNN
	1    6150 1600
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 60CBF7F5
P 7150 1700
F 0 "C3" H 7242 1746 50  0000 L CNN
F 1 "200u" H 7242 1655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal" H 7150 1700 50  0001 C CNN
F 3 "~" H 7150 1700 50  0001 C CNN
	1    7150 1700
	1    0    0    -1  
$EndComp
$Comp
L Diode:MBR745 D10
U 1 1 60CB9DD0
P 6600 1750
F 0 "D10" V 6554 1829 50  0000 L CNN
F 1 "MBR745" V 6645 1829 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-2_Vertical" H 6600 1575 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MBR735-D.PDF" H 6600 1750 50  0001 C CNN
	1    6600 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 1500 6600 1500
Wire Wire Line
	6600 1500 7150 1500
Wire Wire Line
	7150 1500 7150 1600
Connection ~ 6600 1500
$Comp
L Timer:LM555 U3
U 1 1 60D61414
P 6700 2500
F 0 "U3" H 7050 2850 50  0000 C CNN
F 1 "LM555" H 6850 2850 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 6700 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm555.pdf" H 6700 2500 50  0001 C CNN
	1    6700 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2300 6100 2300
$Comp
L Device:R_Small_US R8
U 1 1 60D8694D
P 7300 2250
F 0 "R8" H 7368 2296 50  0000 L CNN
F 1 "20" H 7368 2205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 7300 2250 50  0001 C CNN
F 3 "~" H 7300 2250 50  0001 C CNN
	1    7300 2250
	1    0    0    -1  
$EndComp
Connection ~ 7200 2700
$Comp
L Device:C_Small C5
U 1 1 60DA96AE
P 7300 2900
F 0 "C5" H 7392 2946 50  0000 L CNN
F 1 "1m" H 7392 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal" H 7300 2900 50  0001 C CNN
F 3 "~" H 7300 2900 50  0001 C CNN
	1    7300 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2900 6700 3000
Wire Wire Line
	6700 3000 7300 3000
$Comp
L Device:R_Small_US R10
U 1 1 60D962DE
P 7500 2700
F 0 "R10" V 7600 2550 50  0000 C CNN
F 1 "20" V 7600 2700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 7500 2700 50  0001 C CNN
F 3 "~" H 7500 2700 50  0001 C CNN
	1    7500 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7200 2500 7300 2500
Wire Wire Line
	7200 2700 7300 2700
Wire Wire Line
	7300 2500 7300 2350
Wire Wire Line
	7300 2800 7300 2700
Wire Wire Line
	7300 2500 7700 2500
Wire Wire Line
	7700 2500 7700 2700
Wire Wire Line
	7700 2700 7600 2700
Connection ~ 7300 2500
Wire Wire Line
	7400 2700 7300 2700
Connection ~ 7300 2700
Wire Wire Line
	6600 1900 6600 1950
Wire Wire Line
	6600 1950 7150 1950
Wire Wire Line
	7150 1950 7150 1800
Wire Wire Line
	7300 2150 7300 2100
Wire Wire Line
	7200 2000 7200 2300
Wire Wire Line
	6150 1800 6150 2000
Wire Wire Line
	7150 1500 7750 1500
Connection ~ 7150 1500
Wire Wire Line
	6000 2150 5400 2150
Wire Wire Line
	6000 2150 6000 3050
Wire Wire Line
	4850 3500 4450 3500
Wire Wire Line
	3650 3500 4050 3500
$Comp
L Device:Battery BT1
U 1 1 60CB25F3
P 2000 2600
F 0 "BT1" V 1950 2350 50  0000 L CNN
F 1 "3" V 2050 2400 50  0000 L CNN
F 2 "Battery:BatteryHolder_Keystone_2468_2xAAA" V 2000 2660 50  0001 C CNN
F 3 "~" V 2000 2660 50  0001 C CNN
	1    2000 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	2350 2600 2400 2600
Wire Wire Line
	1700 2500 2750 2500
Wire Wire Line
	4600 3200 4600 4050
Wire Wire Line
	7750 4750 7750 3200
Wire Wire Line
	6000 4750 7750 4750
Wire Wire Line
	5800 3800 5800 3700
Wire Wire Line
	5950 3200 5800 3200
Connection ~ 6600 3650
Connection ~ 6700 4700
Wire Wire Line
	6050 3650 6600 3650
Wire Wire Line
	6050 4700 6050 3650
Wire Wire Line
	6700 4700 6050 4700
Wire Wire Line
	6100 4650 7200 4650
Wire Wire Line
	6100 4000 6100 4650
Wire Wire Line
	7200 4650 7200 4400
$Comp
L Transistor_FET:DMG2302U Q3
U 1 1 60D40ABF
P 6150 3300
F 0 "Q3" H 6000 3450 50  0000 C CNN
F 1 "DMG2302U" H 6000 3550 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6350 3225 50  0001 L CIN
F 3 "http://www.diodes.com/assets/Datasheets/DMG2302U.pdf" H 6150 3300 50  0001 L CNN
	1    6150 3300
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 60D40AC5
P 7150 3400
F 0 "C4" H 7242 3446 50  0000 L CNN
F 1 "200u" H 7242 3355 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal" H 7150 3400 50  0001 C CNN
F 3 "~" H 7150 3400 50  0001 C CNN
	1    7150 3400
	1    0    0    -1  
$EndComp
$Comp
L Diode:MBR745 D11
U 1 1 60D40ACB
P 6600 3450
F 0 "D11" V 6554 3529 50  0000 L CNN
F 1 "MBR745" V 6645 3529 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-2_Vertical" H 6600 3275 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MBR735-D.PDF" H 6600 3450 50  0001 C CNN
	1    6600 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 3200 6600 3200
Wire Wire Line
	6600 3200 6600 3300
Wire Wire Line
	6600 3200 7150 3200
Wire Wire Line
	7150 3200 7150 3300
Connection ~ 6600 3200
$Comp
L Timer:LM555 U4
U 1 1 60D40AD6
P 6700 4200
F 0 "U4" H 7050 4550 50  0000 C CNN
F 1 "LM555" H 6850 4550 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 6700 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm555.pdf" H 6700 4200 50  0001 C CNN
	1    6700 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4000 6100 4000
$Comp
L Device:R_Small_US R9
U 1 1 60D40ADD
P 7300 3950
F 0 "R9" H 7368 3996 50  0000 L CNN
F 1 "20" H 7368 3905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 7300 3950 50  0001 C CNN
F 3 "~" H 7300 3950 50  0001 C CNN
	1    7300 3950
	1    0    0    -1  
$EndComp
Connection ~ 7200 4400
$Comp
L Device:C_Small C6
U 1 1 60D40AE4
P 7300 4600
F 0 "C6" H 7392 4646 50  0000 L CNN
F 1 "1m" H 7392 4555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal" H 7300 4600 50  0001 C CNN
F 3 "~" H 7300 4600 50  0001 C CNN
	1    7300 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4600 6700 4700
Wire Wire Line
	6700 4700 7300 4700
$Comp
L Device:R_Small_US R11
U 1 1 60D40AEC
P 7500 4400
F 0 "R11" V 7600 4250 50  0000 C CNN
F 1 "20" V 7600 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 7500 4400 50  0001 C CNN
F 3 "~" H 7500 4400 50  0001 C CNN
	1    7500 4400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7200 4200 7300 4200
Wire Wire Line
	7200 4400 7300 4400
Wire Wire Line
	7300 4200 7300 4050
Wire Wire Line
	7300 4500 7300 4400
Wire Wire Line
	7300 4200 7700 4200
Wire Wire Line
	7700 4200 7700 4400
Wire Wire Line
	7700 4400 7600 4400
Connection ~ 7300 4200
Wire Wire Line
	7400 4400 7300 4400
Connection ~ 7300 4400
Wire Wire Line
	6600 3600 6600 3650
Wire Wire Line
	6600 3650 7150 3650
Wire Wire Line
	7150 3650 7150 3500
Wire Wire Line
	7300 3850 7300 3700
Wire Wire Line
	6700 3700 7300 3700
Wire Wire Line
	6700 3800 6700 3700
Wire Wire Line
	7200 3750 7200 4000
Wire Wire Line
	6150 3500 6150 3750
Wire Wire Line
	6150 3750 7200 3750
Wire Wire Line
	7150 3200 7750 3200
Connection ~ 7150 3200
Wire Wire Line
	6000 3850 6000 4750
Wire Wire Line
	5350 3800 5800 3800
Wire Wire Line
	4600 4050 5350 4050
Wire Wire Line
	5350 4050 5350 3800
Wire Wire Line
	5000 3850 5000 3200
Wire Wire Line
	5000 3200 5100 3200
Wire Wire Line
	5000 3850 6000 3850
Wire Wire Line
	5700 3100 5700 3900
Wire Wire Line
	5700 3900 5950 3900
Wire Wire Line
	5950 3900 5950 4800
Wire Wire Line
	6600 1500 6600 1600
Wire Wire Line
	5300 4000 5900 4000
Wire Wire Line
	5900 4000 5900 4850
Connection ~ 5300 4000
Wire Wire Line
	5950 4800 8550 4800
$Comp
L Connector:Conn_01x01_Female J4
U 1 1 60E0A864
P 8750 4800
F 0 "J4" H 8778 4826 50  0000 L CNN
F 1 "Conn_01x01_Female" H 8778 4735 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Vertical" H 8750 4800 50  0001 C CNN
F 3 "~" H 8750 4800 50  0001 C CNN
	1    8750 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 3700 5800 3700
Connection ~ 6700 3700
Connection ~ 5800 3700
Wire Wire Line
	5800 3700 5800 3200
$Comp
L power:GND #PWR0101
U 1 1 60E33B02
P 7400 1950
F 0 "#PWR0101" H 7400 1700 50  0001 C CNN
F 1 "GND" H 7405 1777 50  0000 C CNN
F 2 "" H 7400 1950 50  0001 C CNN
F 3 "" H 7400 1950 50  0001 C CNN
	1    7400 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 1950 7400 1950
Connection ~ 7150 1950
$Comp
L power:GND #PWR0102
U 1 1 60E3FD75
P 7400 3650
F 0 "#PWR0102" H 7400 3400 50  0001 C CNN
F 1 "GND" H 7405 3477 50  0000 C CNN
F 2 "" H 7400 3650 50  0001 C CNN
F 3 "" H 7400 3650 50  0001 C CNN
	1    7400 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3650 7400 3650
Connection ~ 7150 3650
Wire Wire Line
	5900 1500 5900 2100
Wire Wire Line
	5900 2100 6700 2100
Wire Wire Line
	6150 2000 7200 2000
Wire Wire Line
	6700 2100 7300 2100
Connection ~ 6700 2100
Wire Wire Line
	4900 1900 4900 2350
Wire Wire Line
	4900 1900 5050 1900
Wire Wire Line
	5050 2100 5050 1900
Connection ~ 5900 2100
Connection ~ 5050 1900
Wire Wire Line
	5050 1900 5200 1900
Wire Wire Line
	5050 2100 5900 2100
NoConn ~ 6200 2700
NoConn ~ 6200 2500
NoConn ~ 6200 4200
NoConn ~ 6200 4400
$Comp
L Connector:USB_C_Plug P2
U 1 1 60BF4EB8
P 8200 2600
F 0 "P2" H 8307 3867 50  0000 C CNN
F 1 "USB_C_Plug" H 8300 3950 50  0000 C CNN
F 2 "Connector_USB:USB_C_Plug_Molex_105444" H 8350 2600 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 8350 2600 50  0001 C CNN
	1    8200 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 4850 7900 4850
Wire Wire Line
	8800 1400 8800 1600
Wire Wire Line
	5800 1400 8800 1400
Wire Wire Line
	7900 4200 7900 4850
Connection ~ 7900 4850
Wire Wire Line
	8200 4200 8200 4850
Wire Wire Line
	7900 4850 8200 4850
NoConn ~ 8800 1800
NoConn ~ 8800 1900
NoConn ~ 8800 2100
NoConn ~ 8800 2300
NoConn ~ 8800 2600
NoConn ~ 8800 2700
NoConn ~ 8800 2900
NoConn ~ 8800 3000
NoConn ~ 8800 3200
NoConn ~ 8800 3300
NoConn ~ 8800 3500
NoConn ~ 8800 3600
NoConn ~ 8800 3800
NoConn ~ 8800 3900
Wire Wire Line
	2200 2600 2350 2600
Connection ~ 2350 2600
Wire Wire Line
	1800 2800 1800 2600
$Comp
L Device:Battery BT2
U 1 1 60CB98A7
P 1800 3000
F 0 "BT2" V 1750 2700 50  0000 L CNN
F 1 "3" V 1850 2750 50  0000 L CNN
F 2 "Battery:BatteryHolder_Keystone_2479_3xAAA" V 1800 3060 50  0001 C CNN
F 3 "~" V 1800 3060 50  0001 C CNN
	1    1800 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2500 1700 3400
Wire Wire Line
	1800 3400 1800 3200
Wire Wire Line
	1700 3400 1800 3400
$Comp
L Transistor_BJT:2N2219 Q1
U 1 1 61018D11
P 3300 2650
F 0 "Q1" H 3490 2696 50  0000 L CNN
F 1 "2N2219" H 3490 2605 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 3500 2575 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 3300 2650 50  0001 L CNN
	1    3300 2650
	0    -1   -1   0   
$EndComp
Text Notes 3150 1600 0    50   ~ 0
Pi Stock Charger
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 60BDED8B
P 3050 1500
F 0 "J1" H 2950 1700 50  0000 R CNN
F 1 "Conn_01x02_Female" H 3300 1600 50  0000 R CNN
F 2 "Connector_JST:JST_EH_S2B-EH_1x02_P2.50mm_Horizontal" H 3050 1500 50  0001 C CNN
F 3 "~" H 3050 1500 50  0001 C CNN
	1    3050 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 1450 2800 1600
Wire Wire Line
	2800 1600 2850 1600
Wire Wire Line
	1650 2050 1800 2050
Wire Wire Line
	1800 2050 1850 2050
Connection ~ 1800 2050
Wire Wire Line
	1800 2050 1800 1450
Wire Wire Line
	1800 1450 2800 1450
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 60C54D66
P 4850 1400
F 0 "J2" V 4900 1800 50  0000 C CNN
F 1 "Conn_01x02_Female" V 4800 1800 50  0000 C CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MC_1,5_2-G-5.08_1x02_P5.08mm_Horizontal" H 4850 1400 50  0001 C CNN
F 3 "~" H 4850 1400 50  0001 C CNN
	1    4850 1400
	0    -1   -1   0   
$EndComp
$Comp
L Device:Battery BT4
U 1 1 60C8CE69
P 4250 3500
F 0 "BT4" V 4400 3500 50  0000 C CNN
F 1 "2" V 4500 3500 50  0000 C CNN
F 2 "Battery:BatteryHolder_Keystone_2466_1xAAA" V 4250 3560 50  0001 C CNN
F 3 "~" V 4250 3560 50  0001 C CNN
	1    4250 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 2350 4900 2350
Wire Wire Line
	4950 1800 4950 2400
Wire Wire Line
	4950 2400 4750 2400
Wire Wire Line
	4750 2400 4750 2700
Wire Wire Line
	4750 2700 4650 2700
Wire Wire Line
	4650 2700 4650 3250
Wire Wire Line
	3650 3500 3650 3250
Wire Wire Line
	3650 3250 4650 3250
Connection ~ 3650 3500
Wire Wire Line
	4850 1600 4850 1800
Wire Wire Line
	4850 1800 4950 1800
Wire Wire Line
	4950 1700 4950 1600
$EndSCHEMATC
