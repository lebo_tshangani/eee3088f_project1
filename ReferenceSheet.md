﻿Reference Sheet


LM555 Timer

Features

- Operating voltage:  +5V to +18V
- Operating temperature:  approx. 70oC


LMV331 Comparator

Features

- Operating voltage:  +2.7 to +5V
- Operating temperature:  approx. 70oC


NPN -BCE TRANSISTOR (BC547) 

Features

- DC Current Gain: 800
- Emitter Base Voltage: 6V
- Base Current:  5mA 


# Bibliography
` `BIBLIOGRAPHY *555 Timer IC*. (2017, August 30). Retrieved from COMPONENTS101: https://components101.com/ics/555-timer-ic-pinout-datasheet

*BC547 Transistor*. (2017, August 30). Retrieved from COMPONENTS101: https://components101.com/transistors/bc547-transistor-pinout-datasheet

*What is BC547 Transistor Working and Its Applications*. (n.d.). Retrieved from ELPROCUS: https://www.elprocus.com/bc547-transistor-working-and-its-applications/





