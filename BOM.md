|Id |Designator      |Package                                               |Quantity|Designation      |
|---|----------------|------------------------------------------------------|--------|-----------------|
|1  |R2,R5           |R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical        |2       |1k               |
|2  |D3,D6,D7,D8     |LED_D3.0mm_Clear                                      |4       |LED              |
|3  |BT1             |BatteryHolder_Keystone_2468_2xAAA                     |1       |3                |
|4  |BT2             |BatteryHolder_Keystone_2479_3xAAA                     |1       |3                |
|5  |BT4             |BatteryHolder_Keystone_2466_1xAAA                     |1       |2                |
|6  |C1              |CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal          |1       |C_Small          |
|7  |C2,C5,C6        |CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal          |3       |1m               |
|8  |C3,C4           |CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal          |2       |200u             |
|9  |D1,D2,D4,D5,D9  |D_5W_P10.16mm_Horizontal                              |5       |DIODE            |
|10 |D10,D11         |TO-220-2_Vertical                                     |2       |MBR745           |
|11 |D_status1       |LED_D3.0mm_FlatTop                                    |1       |LED              |
|12 |J1              |JST_EH_S2B-EH_1x02_P2.50mm_Horizontal                 |1       |Conn_01x02_Female|
|13 |J2              |PhoenixContact_MC_1,5_2-G-5.08_1x02_P5.08mm_Horizontal|1       |Conn_01x02_Female|
|14 |J4              |PinHeader_1x01_P1.00mm_Vertical                       |1       |Conn_01x01_Female|
|15 |P1              |PhoenixContact_MC_1,5_2-G-5.08_1x02_P5.08mm_Horizontal|1       |Conn_WallPlug    |
|16 |P2              |USB_C_Plug_Molex_105444                               |1       |USB_C_Plug       |
|17 |Q1              |TO-39-3                                               |1       |2N2219           |
|18 |Q2,Q3           |SOT-23                                                |2       |DMG2302U         |
|19 |R1,R8,R9,R10,R11|R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical        |5       |20               |
|20 |R3              |R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical        |1       |100              |
|21 |R4              |R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical        |1       |75               |
|22 |R6              |R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical        |1       |50               |
|23 |R7              |R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical        |1       |25               |
|24 |T1              |Transformer_Toroid_Horizontal_D9.0mm_Amidon-T30       |1       |Transformer_1P_1S|
|25 |U1,U2           |SOT-23-5                                              |2       |LMV331           |
|26 |U3,U4           |DIP-8_W7.62mm_LongPads                                |2       |LM555            |
